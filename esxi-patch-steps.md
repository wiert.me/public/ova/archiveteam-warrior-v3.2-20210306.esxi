# `archiveteam-warrior-v3.2-20210306.ova` for ESXi

## Patched `archiveteam-warrior-v3.2-20210306.ova` for ESXi

This directory conains the already patched `archiveteam-warrior-v3.2-20210306.ova` for ESXi.

Steps below explain how to generate it so you can reproduce what I did.

A decompressed version is already in directory `archiveteam-warrior-v3.2-20210306.ESXi`, see the notes below.

## Patching `archiveteam-warrior-v3.2-20210306.ova` for ESXi

Execute this script on Linux or MacOS:

``` bash
curl --remote-name https://warriorhq.archiveteam.org/downloads/warrior3/README.txt
curl --remote-name https://warriorhq.archiveteam.org/downloads/warrior3/sha256sum.txt
curl --remote-name https://warriorhq.archiveteam.org/downloads/warrior3/archiveteam-warrior-v3.2-20210306.ova
curl --remote-name https://warriorhq.archiveteam.org/downloads/warrior3/archiveteam-warrior-v3.2-20210306.ova.asc
shasum --check sha256sum.txt 
rm -rf archiveteam-warrior-v3.2-20210306.ESXi
mkdir archiveteam-warrior-v3.2-20210306.ESXi
pushd archiveteam-warrior-v3.2-20210306.ESXi
tar xvf ../archiveteam-warrior-v3.2-20210306.ova
curl --remote-name https://gist.githubusercontent.com/jpluimers/a572db71bfc767dc5bae045ebdc798d2/raw/63bc13674cde43b412420ae6efff6b80e53b0ccc/archiveteam-warrior-v3.2-20210306.ovf.patch
patch < archiveteam-warrior-v3.2-20210306.ovf.patch
shasum --algorithm 256 archiveteam-warrior-v3.2-20210306.ovf archiveteam-warrior-v3.2-20210306-disk001.vmdk > sha256sum.txt
tar zcfv ../archiveteam-warrior-v3.2-20210306.ova archiveteam-warrior-v3.2-20210306.ovf archiveteam-warrior-v3.2-20210306-disk001.vmdk
popd
rm archiveteam-warrior-v3.2-20210306.ova.asc
shasum --algorithm 256 archiveteam-warrior-v3.2-20210306.ova > sha256sum.txt
```

The above steps are based on:

1. [www.archiveteam.org/index.php?title=Deploy_OVA_to_VMware_ESXi](https://www.archiveteam.org/index.php?title=Deploy_OVA_to_VMware_ESXi)
2. [edvoncken.net/2014/08/archiveteam-warrior-on-esxi](https://edvoncken.net/2014/08/archiveteam-warrior-on-esxi/)
3. [gist.github.com/Kipari/c32192de52c2a86d56ee0d67472d4dc6](https://gist.github.com/Kipari/c32192de52c2a86d56ee0d67472d4dc6)

## Uncompressed patched files in the  `archiveteam-warrior-v3.2-20210306.ESXi` directory

In my experience, ESXi sometimes has problems uploading `.ova` files when creating a new virtual machine. In stead of uploading a compressed `.ova` file, it often works better during the creation of the new VM to upload the `.ovf` and `.vmdk` files.

So if you want to create an Archive Warrior v3.2 VM, just upload the patched `archiveteam-warrior-v3.2-20210306.ESXi` directory files:

- archiveteam-warrior-v3.2-20210306.ovf
- archiveteam-warrior-v3.2-20210306-disk001.vmdk

That's why I will I have added that patched directory to the git repository (including `sha256sum.txt`).

