Warrior Virtual Machine Appliance Version 3 Downloads
=====================================================

This directory contains the VM appliances for the Warrior.

The files are named as so:

    archiveteam-warrior-v3-YYYYMMDD.ova

To find the latest release, simply look for the date in the filename.

The latest version is "3.2".

Each appliance may have an signature file (asc file) next to it. They 
can be used to verify the file using GnuGP. Additionally, SHA-256 digests
are provided if you wish to verify using sha256sum command.

For details on how to use the appliance, please see the wiki at
https://www.archiveteam.org/index.php?title=ArchiveTeam_Warrior

Troubleshooting
---------------

Ensure that you have the latest appliance downloaded. If you continue
to get a blank page for some projects, this means the project is not 
supported within the appliance. This issue will be properly addressed 
in the future.

You can run the projects directly using Docker however. For further info, 
see our GitHub repository for each project's Readme instructions. 

If you have any issues or feedback, chat on #warrior on hackint.

Additional GPG info
-------------------

The key IDs can be found at:

* https://api.github.com/users/chfoo/gpg_keys

Public keys can be fetched from public key servers.

Additional download links
-------------------------

* https://github.com/ArchiveTeam/Ubuntu-Warrior/releases
